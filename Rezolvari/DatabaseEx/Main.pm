package Main;
use strict;
use warnings;
no warnings 'experimental';
use Data::Dumper;
use feature ':5.12';

use DatabaseOps;
use DatabaseConnection;
my $dbh = connect();

printMenu();

sub printMenu{
    while (1) {
        say ("===SCRABBLE GAME===");
        say ("1. User profile");
        say ("2. User highest score");
        say ("3. Leaderboard screen");
        say ("4. EXIT");
        say ("Please enter your option: ");
        chomp (my $cmd = <STDIN>);
        given ($cmd) {
            when("1"){
                say "Enter the member id: ";
                chomp (my $id = <STDIN>);
                get_user_profile($id, $dbh);
            }
            when("2"){
                say "Enter the member id: ";
                chomp (my $id = <STDIN>);
                get_user_best_score($id, $dbh);
            }
            when("3"){
                get_leaderboard_screen($dbh);
            }
            when("4"){
                say "Goodbye!";
                last;
            }
            default{
                say "Wrong option!";
            }
        }
    }
}

$dbh->disconnect();

1;