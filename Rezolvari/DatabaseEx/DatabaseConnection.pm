package DatabaseConnection;

use strict;
use warnings;
use v5.10;
use DBI;

use base ('Exporter');
our @EXPORT = qw(connect);

sub connect{
    my $dsn = "DBI:mysql:perl_database";
    my $username = "valentina";
    my $password = 'password';
    
    my %attr = ( PrintError=>0, RaiseError=>1 );            
    my $dbh  = DBI->connect($dsn,$username,$password, \%attr);
    say "Connected to the MySQL database.";
    return $dbh;
}

1;