package DatabaseOps;
use strict;
use warnings;
use Data::Dumper;
use feature ':5.12';

use base ('Exporter');
our @EXPORT = qw(get_user_profile get_user_best_score get_leaderboard_screen);

sub get_user_profile{
    my $user_id = shift;
    my $dbh = shift;
    my $sql = 
    "
    SELECT
    lost_sum.ID,
    CONCAT(m.first_name, ' ', m.last_name) as NAME,
    lost_sum.NO_LOST,
    win_sum.NO_WINS, 
    CASE
        WHEN (lost_sum.NO_LOST + win_sum.NO_WINS) != 0
            THEN (lost_sum.SUM_LOST + win_sum.SUM_WIN) / (lost_sum.NO_LOST + win_sum.NO_WINS)
        ELSE 0
    END
    FROM
    (
        SELECT 
        CASE WHEN looser_id IS NULL THEN ? ELSE looser_id END as ID,
        COUNT(looser_id) as NO_LOST, 
        CASE WHEN SUM(looser_score) IS NULL THEN 0 ELSE SUM(looser_score) END as SUM_LOST
        FROM games
        WHERE looser_id = ?
    ) lost_sum,
    (
        SELECT
        CASE WHEN winner_id IS NULL THEN ? ELSE winner_id END, 
        COUNT(winner_id) as NO_WINS, 
        CASE WHEN SUM(winner_score) IS NULL THEN 0 ELSE SUM(winner_score) END as SUM_WIN 
        FROM games
        WHERE winner_id = ?
    ) win_sum,
    members m
    WHERE m.member_id = lost_sum.ID;
    ";

    my $sth = $dbh->prepare($sql); 
    $sth->execute($user_id, $user_id, $user_id, $user_id);

    my $found = 0;
    while (my @row = $sth->fetchrow_array()){
        $found = 1;
        printf("Id: %s; Name: %s; No. lost: %s; No. win: %s; Average score: %s;\n",$row[0],$row[1],$row[2],$row[3],$row[4]);
    }
    if ($found == 0){
        say "This member does not exist in our database!";
    }
    $sth->finish();
}

sub get_user_best_score{
    my $user_id = shift;
    my $dbh = shift;
    my $sql = 
    "
    SELECT 
    g.winner_id,
    MAX(winner_score) as BEST_WIN, 
    g.looser_id, 
    CONCAT(m.first_name, ' ', m.last_name) as LOOSER_NAME
    FROM games g LEFT OUTER JOIN members m on g.looser_id = m.member_id
    WHERE g.winner_id = ?
    GROUP BY g.winner_id, g.looser_id
    ORDER BY BEST_WIN DESC
    LIMIT 1
    ";

    my $sth = $dbh->prepare($sql); 
    $sth->execute($user_id);
    my $found = 0;
    while(my @row = $sth->fetchrow_array()){
        $found = 1;
        printf("Id: %s; Best Score: %s; Against member with Id: %s and Name: %s;\n",$row[0],$row[1],$row[2],$row[3]);
    } 
    if($found ==0 ){
        say "This member has'n won any games so far or doesn't exist in our database!";
    }

    $sth->finish();
}

sub get_leaderboard_screen{
    my $dbh = shift;
    my $sql = 
    "
    SELECT
    member_id,
    CONCAT(m.first_name, ' ', m.last_name) as NAME,
    lost_sum.NO_LOST,
    win_sum.NO_WINS, 
    CASE
        WHEN (lost_sum.NO_LOST + win_sum.NO_WINS) != 0
            THEN (lost_sum.SUM_LOST + win_sum.SUM_WIN) / (lost_sum.NO_LOST + win_sum.NO_WINS)
        ELSE 0
    END AS AVERAGE_SCORE
    FROM
    (
        SELECT 
        member_id as ID,
        COUNT(looser_id) as NO_LOST, 
        CASE WHEN SUM(looser_score) IS NULL THEN 0 ELSE SUM(looser_score) END as SUM_LOST
        FROM members left outer join games on member_id = looser_id
        GROUP BY member_id
    ) lost_sum,
    (
        SELECT
        member_id as ID,
        COUNT(winner_id) as NO_WINS, 
        CASE WHEN SUM(winner_score) IS NULL THEN 0 ELSE SUM(winner_score) END as SUM_WIN 
        FROM members left outer join games on member_id = winner_id
        GROUP BY member_id
    ) win_sum,
    members m
    WHERE m.member_id = lost_sum.ID 
    AND m.member_id = win_sum.ID 
    AND lost_sum.NO_LOST + win_sum.NO_WINS >= 3 
    ORDER BY AVERAGE_SCORE DESC
    LIMIT 3
    ";

    my $sth = $dbh->prepare($sql); 
    $sth->execute();
    while(my @row = $sth->fetchrow_array()){
        printf("Id: %s; Name: %s; No. lost: %s; No wins: %s; AVERAGE: %s;\n",$row[0],$row[1],$row[2],$row[3],$row[4]);
    } 
    $sth->finish();
}

1;