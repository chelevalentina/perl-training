package TableCreation;
use strict;
use warnings;
use v5.10;
use DBI;

use DatabaseConnection;
my $dbh = connect();

my @ddl = ("CREATE TABLE IF NOT EXISTS members (member_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
                                 joining_date VARCHAR(255),
                                 first_name VARCHAR(255),
                                 last_name VARCHAR(255),
                                 home_city VARCHAR(255)) ENGINE=InnoDB;",
           "CREATE TABLE IF NOT EXISTS games (game_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
                                winner_id INT NOT NULL,
                                looser_id INT NOT NULL,
                                winner_score INT,
                                looser_score INT,
                                CONSTRAINT fk_winner FOREIGN KEY (winner_id) 
                                REFERENCES members (member_id),
                                CONSTRAINT fk_looser FOREIGN KEY (looser_id) 
                                REFERENCES members (member_id))
                                ENGINE=InnoDB;");
for my $sql(@ddl){
    $dbh->do($sql);
}
say "The tables have been successfully created!";

$dbh->disconnect();

1;