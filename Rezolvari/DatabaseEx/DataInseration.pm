package DataInseration;
use strict;
use warnings;
use Data::Dumper;
use Parse::CSV;
use feature ':5.12';

use base ('Exporter');
our @EXPORT = qw(get_members_from_file get_games_from_file);

use DatabaseConnection;
my $dbh = connect();

my $members_array_ref = get_members_from_file();
my $games_array_ref = get_games_from_file();

my $sql = "INSERT INTO members(joining_date,first_name,last_name,home_city) VALUES(?,?,?,?)";
my $stmt = $dbh->prepare($sql);
foreach my $member(@{$members_array_ref}){
    if($stmt->execute($member->[0], $member->[1], $member->[2], $member->[3])){
        say "Member inserted successfully!"
    }
}
$stmt->finish();

$sql = "INSERT INTO games(winner_id,looser_id,winner_score,looser_score) VALUES(?,?,?,?)";
$stmt = $dbh->prepare($sql);
foreach my $game(@{$games_array_ref}){
    if($stmt->execute($game->[0], $game->[1], $game->[2], $game->[3])){
        say "Game inserted successfully!";
    }
}
$stmt->finish();
$dbh->disconnect();

sub get_members_from_file{
    my @members_array = ();
    my $members = Parse::CSV->new(
        file => 'members.csv',
    ); 
    while ( my $members_ref = $members->fetch ) {
        push @members_array, $members_ref;
    }
    return \@members_array;
}

sub get_games_from_file{
    my @games_array = ();
    my $games = Parse::CSV->new(
        file => 'games.csv',
    ); 
    while ( my $games_ref = $games->fetch ) {
        push @games_array, $games_ref;
    }
    return \@games_array;
}

1;