#!/usr/bin/perl
use strict;
use warnings;
use Data::Dumper;
use feature ':5.12';
use List::MoreUtils qw(uniq);
no warnings 'experimental';

our $students_ref = [];
our $group_A_students_ref = [];
our $group_B_students_ref = [];

&read_students_from_file;

@{$group_A_students_ref} = (grep {$_->{group} eq 'A'} @{$students_ref});
@{$group_B_students_ref} = (grep {$_->{group} eq 'B'} @{$students_ref});
print "Students A:".Dumper($group_A_students_ref);
print "Students B:".Dumper($group_B_students_ref);

my @sorted_students = &get_the_best_students;
print "The first 3 students:".Dumper(\@sorted_students);

my $group_A_average_grade = &get_the_average_grade(@{$group_A_students_ref});
my $group_B_average_grade = &get_the_average_grade(@{$group_B_students_ref});
my $school_average_grade = &get_the_average_grade(@{$students_ref});
print "Group A average:".Dumper($group_A_average_grade);
print "Group B average:".Dumper($group_B_average_grade);
print "School average:".Dumper($school_average_grade);

my $best_group_name = &get_the_best_group_name($group_A_average_grade, $group_B_average_grade);
print "The best group:".Dumper($best_group_name);

my @all_usernames = &get_all_usernames;
print "All usernames:".Dumper(\@all_usernames);

my $domains_count = &get_email_domains_groups;
print "Domains count:".Dumper($domains_count);

sub read_students_from_file{
    open my $fh, '<', 'students.txt';
    while(<$fh>){
        chomp;
        my $student = $_;
        my @splitted_student = split ("," , $student);
        my $group = $splitted_student[5];
        my $student_hash_ref = {id => $splitted_student[0],
                                first_name => $splitted_student[1],
                                last_name => $splitted_student[2],
                                email => $splitted_student[3],
                                grade => $splitted_student[4],
                                group => $group,
                                ip_addr => $splitted_student[6]
                                };
        push @{$students_ref}, $student_hash_ref;
    }
}

sub get_the_best_students{
    my @sorted_students = sort {$b->{grade} cmp $a->{grade}} (@{$group_A_students_ref}, @{$group_B_students_ref});
    my @names = map {{
                    "first_name" => $_->{first_name}, 
                    "last_name" => $_->{last_name}, 
                    "grade" => $_->{grade}
                    }} @sorted_students;
    splice @names, 0, 3;
}

sub get_the_average_grade{
    my ($grade_sum, $grade_no) = (0, 0);
    my @grades = (map {$_->{grade}} @_);
    foreach my $grade (@grades){
        $grade_sum+= $grade;
        $grade_no++;
    } 
    $grade_sum/$grade_no;
}

sub get_the_best_group_name{
    if($_[0] == $_[1]) {
        return "The averages are equal.";
    }
    $_[0] >= $_[1] ? "A" : "B";
}

sub get_all_usernames{
    my @usernames;
    my @all_emails = map {$_->{email}} @{$students_ref};
    foreach my $email(@all_emails){
        $email =~  m/(^.*)@.*/ ;
        push @usernames, $1; 
    }
    @usernames;
}

sub get_email_domains_groups{
    my @domains;
    my @all_emails = map {$_->{email}} @{$students_ref};
    foreach my $email(@all_emails){
        $email =~  m/^.*@(.*)\.(.*$)/ ;
        push @domains, $1; 
    }
    my $domains_count = {};
    foreach (uniq @domains){
        $domains_count->{$_} = 0;
    }
    foreach (@domains){
        $domains_count->{$_} += 1;
    }
    $domains_count;
}