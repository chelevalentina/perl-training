use Schedule::Cron;
use strict;
use warnings;
use feature ':5.12';

my $hostname = `hostname`;
my $this_day = `date`;

use Email::Simple;
use Email::Simple::Creator;
use Email::Sender::Simple qw(sendmail);

my $cron = new Schedule::Cron(\&dispatcher);
$cron->add_entry("0-59/5 * * * *", \&send_email);
$cron->run(detach=>1, pid_file=>"scheduler.pid");

sub send_email{
    my $email = Email::Simple->create(
    header => [
        From => "admin\@$hostname", 
        To => "valentina.chele\@devnest.ro", 
        Subject => "SCHEDULE COMPLETE - $this_day"
        ],
    body => "Have a nice day..."
    );
    sendmail($email);
}

