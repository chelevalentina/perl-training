#!/usr/bin/perl
use strict;
use warnings;

&convert_time;

sub convert_time{
    my ($input_time, $output_time);
    print "Insert the time: ";
    chomp ($input_time = <STDIN>);
    if($input_time =~ /(0[1-9]|1[0-2]):([0-5][0-9]):([0-5][0-9])(PM|AM)/i){
        print "The format is OK!\n";
        my ($hour, $minute, $second, $meridiem) =  ($1, $2, $3, $4);
        my $new_hour;
        print "Hour: " . $hour . ", Minute: " . $minute . ", Second: " . $second . ", Meridiem: " . $meridiem . "\n";
        if($meridiem =~ /am/i){
            if ($hour == "12"){
                $new_hour = "00";
                $output_time = "${new_hour}:${minute}:${second}"; 
            }
            else{
                $output_time = substr($input_time, 0, 8);
            }
        }
        else{
            if($hour == "12"){
                $output_time = substr($input_time, 0, 8);
            }
            else{
                $new_hour = $hour + 12;
                $output_time = "${new_hour}:${minute}:${second}";
            }
        }
        print "The new time format: " . $output_time;
    }
    else{
        print "The format is not OK!";
    }
}