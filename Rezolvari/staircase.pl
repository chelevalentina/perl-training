#!/usr/bin/perl
use strict;
use warnings;

&make_staircase;

sub make_staircase{
    print "Give the number of stairs: ";
    chomp (my $number = <STDIN>);
    my $contor = $number;
    while ($contor > 0){
        print " " x $contor . "#" x ($number - $contor + 1) . "\n";
        $contor--;
    }
}
