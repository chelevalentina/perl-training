package Main;
use strict;
use warnings;
no warnings 'experimental';
use Data::Dumper;
use feature ':5.12';

use DatabaseOps;

printMenu();

sub printMenu{
    while (1) {
        say ("===SCRABBLE GAME===");
        say ("1. User profile");
        say ("2. User highest score");
        say ("3. Leaderboard screen");
        say ("4. EXIT");
        say ("Please enter your option: ");
        chomp (my $cmd = <STDIN>);
        given ($cmd) {
            when("1"){
                say "Enter the member id: ";
                chomp (my $id = <STDIN>);
                my $user = get_user_profile($id);
                say "Name: " . $user->{first_name} . " " . $user->{last_name} . "\n" . 
                    "No. of lost games: " . $user->{lost_count} . "\n" .  
                    "No. of won games: " . $user->{win_count} . "\n" . 
                    "Average score: " . $user->{average};   
            }
            when("2"){
                say "Enter the member id: ";
                chomp (my $id = <STDIN>);
                get_user_best_score($id);
            }
            when("3"){
                get_leaderboard_screen();
            }
            when("4"){
                say "Goodbye!";
                last;
            }
            default{
                say "Wrong option!";
            }
        }
    }
}
1;