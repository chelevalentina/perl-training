package DatabaseOps;

use strict;
use warnings;
use v5.10;
use DBI;
use Data::Dumper;
use Scrabble::Schema;

use base ('Exporter');
our @EXPORT = qw(get_user_profile get_user_best_score get_leaderboard_screen);

our $schema = Scrabble::Schema->connect('dbi:mysql:dbname=perl_database','root','');

sub get_user_profile{
    my $user_id = shift;
    my $user = {};
    my $user_rs = $schema->resultset("Member")->search({
        member_id => $user_id
    });
    my $lost_rs = $schema->resultset("Game")->search({
        looser_id => $user_id
    });
    my ($lost_sum, $lost_count) = (0,0);
    while (my $game = $lost_rs->next){
        $lost_sum += $game->looser_score;
        $lost_count++;
    }

    my $win_rs = $schema->resultset("Game")->search({
        winner_id => $user_id
    });
    my ($win_sum, $win_count) = (0,0);
    while (my $game = $win_rs->next){
        $win_sum += $game->winner_score;
        $win_count++;
    }
    my $average = 0;
    my $total_games_played = $win_count + $lost_count;
    my $total_sum = $win_sum + $lost_sum;
    if ($total_games_played > 0){
        $average = ($total_sum/$total_games_played);
    }
    $user->{id} = $user_id;
    $user->{first_name} = $user_rs->first->first_name;
    $user->{last_name} = $user_rs->first->last_name;
    $user->{lost_count} = $lost_count;
    $user->{win_count} = $win_count;
    $user->{play_count} = $lost_count + $win_count;
    $user->{average} = $average; 
    return $user;  
}

sub get_user_best_score{
    my $user_id = shift;
    my ($win_score, $lost_score) = (0,0);
    my $game_rs = $schema->resultset('Game')->search(
        {
            winner_id => $user_id
        },
        {
            order_by => {-desc => 'winner_score'}
        }
    );
    if(defined $game_rs->first){
        my ($looser_id, $winner_name, $looser_name);
        $win_score = $game_rs->first->winner_score;
        $looser_id = $game_rs->first->looser_id;
        my $winner_rs = $schema->resultset('Member')->search({
            member_id => $user_id
        });
        my $looser_rs = $schema->resultset('Member')->search({
            member_id => $looser_id
        });
        say "Name: " . $winner_rs->first->first_name . " " . $winner_rs->first->last_name . "\n" .
            "Best score: " . $win_score . "\n" .
            "Looser ID: " . $looser_id . "\n" .
            "Looser name: " . $looser_rs->first->first_name . " " . $looser_rs->first->last_name; 
    }
    else{
        say "This user hasn't won any games so far or doesn't exist in our database.";
    }
}

sub get_leaderboard_screen{
    my $member_rs = $schema->resultset('Member');
    my $users =[];
    while(my $member = $member_rs->next){
        my $user_profile = get_user_profile($member->member_id);
        push $users, $user_profile;
    }
    my @users_array = grep { $_->{play_count} >= 4 } @{$users};
    my @sorted =  sort { $b->{average} <=> $a->{average} } @users_array;
    my $count = 1;
    while($count <= 3 && defined $sorted[$count-1]){
        say "PLACE " . $count . "\n" .
            "Id: " . $sorted[$count-1]->{id} . "\n" . 
            "First Name: " . $sorted[$count-1]->{first_name} . "\n" .
            "Last Name: " . $sorted[$count-1]->{last_name} . "\n" .
            "No. of played games: " . $sorted[$count-1]->{play_count} . "\n" .
            "Average score: " . $sorted[$count-1]->{average} . "\n";
        $count++;
    }
}

1;