package TableCreation;

use strict;
use warnings;
use v5.10;
use DBI;
use DBIx::Insert::Multi;
use Parse::CSV;
use Data::Dumper;

use base ('Exporter');
our @EXPORT = qw(connect);

my $members_array_ref = get_members_from_file();
my $games_array_ref = get_games_from_file();

my $dbh = connect_to_database();
insert_into_database($dbh, $members_array_ref, $games_array_ref);

sub connect_to_database{
    my $dsn = "DBI:mysql:perl_database";
    my $user = "root";
    my $password = ""; 
    my $dbh = DBI->connect($dsn, $user, $password);
    return $dbh;
}

sub insert_into_database{
    my ($dbh, $members_array_ref, $games_array_ref) = (shift, shift, shift);
    my $multi = DBIx::Insert::Multi->new({dbh => $dbh});
    $multi->insert(
        members => $members_array_ref
    );
    $multi->insert(
        games => $games_array_ref
    );
}

sub get_members_from_file{
    my @members_array = ();
    my $members = Parse::CSV->new(
        file => 'members.csv',
    ); 
    while ( my $member = $members->fetch ) {
        my $member_hash_ref = {};
        $member_hash_ref->{joining_date} = $member->[0];
        $member_hash_ref->{first_name} = $member->[1];
        $member_hash_ref->{last_name} = $member->[2];
        $member_hash_ref->{home_city} = $member->[3];
        push @members_array, $member_hash_ref;
    }
    return \@members_array;
}

sub get_games_from_file{
    my @games_array = ();
    my $games = Parse::CSV->new(
        file => 'games.csv',
    ); 
    while ( my $game = $games->fetch ) {
        my $game_hash_ref = {};
        $game_hash_ref->{winner_id} = $game->[0];
        $game_hash_ref->{looser_id} = $game->[1];
        $game_hash_ref->{winner_score} = $game->[2];
        $game_hash_ref->{looser_score} = $game->[3];
        push @games_array, $game_hash_ref;
    }
    return \@games_array;
}

1;