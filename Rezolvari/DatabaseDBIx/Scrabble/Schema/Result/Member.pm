use utf8;
package Scrabble::Schema::Result::Member;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Scrabble::Schema::Result::Member

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<members>

=cut

__PACKAGE__->table("members");

=head1 ACCESSORS

=head2 member_id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 joining_date

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 first_name

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 last_name

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 home_city

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=cut

__PACKAGE__->add_columns(
  "member_id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "joining_date",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "first_name",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "last_name",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "home_city",
  { data_type => "varchar", is_nullable => 1, size => 255 },
);

=head1 PRIMARY KEY

=over 4

=item * L</member_id>

=back

=cut

__PACKAGE__->set_primary_key("member_id");

=head1 RELATIONS

=head2 games_looser

Type: has_many

Related object: L<Scrabble::Schema::Result::Game>

=cut

__PACKAGE__->has_many(
  "games_looser",
  "Scrabble::Schema::Result::Game",
  { "foreign.looser_id" => "self.member_id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 games_winners

Type: has_many

Related object: L<Scrabble::Schema::Result::Game>

=cut

__PACKAGE__->has_many(
  "games_winners",
  "Scrabble::Schema::Result::Game",
  { "foreign.winner_id" => "self.member_id" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2020-01-20 14:00:13
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:ymj+ZHAEMfll99NPE2Sa+Q


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
