use utf8;
package Scrabble::Schema::Result::Game;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Scrabble::Schema::Result::Game

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<games>

=cut

__PACKAGE__->table("games");

=head1 ACCESSORS

=head2 game_id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 winner_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 looser_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 winner_score

  data_type: 'integer'
  is_nullable: 1

=head2 looser_score

  data_type: 'integer'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "game_id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "winner_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "looser_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "winner_score",
  { data_type => "integer", is_nullable => 1 },
  "looser_score",
  { data_type => "integer", is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</game_id>

=back

=cut

__PACKAGE__->set_primary_key("game_id");

=head1 RELATIONS

=head2 looser

Type: belongs_to

Related object: L<Scrabble::Schema::Result::Member>

=cut

__PACKAGE__->belongs_to(
  "looser",
  "Scrabble::Schema::Result::Member",
  { member_id => "looser_id" },
  { is_deferrable => 1, on_delete => "RESTRICT", on_update => "RESTRICT" },
);

=head2 winner

Type: belongs_to

Related object: L<Scrabble::Schema::Result::Member>

=cut

__PACKAGE__->belongs_to(
  "winner",
  "Scrabble::Schema::Result::Member",
  { member_id => "winner_id" },
  { is_deferrable => 1, on_delete => "RESTRICT", on_update => "RESTRICT" },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2020-01-20 14:00:13
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:AVwPwjTkL9q9qX7vYXQNWA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
