#!/usr/bin/perl
use strict;
use warnings;
use Data::Dumper;

my $students = [
    {
        id => 1,
        first_name => 'Jane',
        last_name => 'Markham',
        email => 'janem@yahoo.com',
        grade => 9.2,
        group => 'A',
        ip_addr => '170.126.23.114',
    },
    {
        id => 2,
        first_name => 'Judith',
        last_name => 'Kirkland',
        email => 'judithk@yahoo.com',
        grade => 5.4,
        group => 'A',
        ip_addr => '38.119.239.231',
    },
    {
        id => 3,
        first_name => 'Allison',
        last_name => 'Ibrar',
        email => 'allisoni@gmail.com',
        grade => 4.2,
        group => 'A',
        ip_addr => '248.166.80.207',
    },
    {
        id => 4,
        first_name => 'Rafi',
        last_name => 'Horton',
        email => 'rafih@yahoo.com',
        grade => 9.2,
        group => 'B',
        ip_addr => '28.21.202.87',
    },
    {
        id => 5,
        first_name => 'Craig',
        last_name => 'Preece',
        email => 'craigp@yahoo.com',
        grade => 8.6,
        group => 'B',
        ip_addr => '85.184.189.120',
    },
    {
        id => 6,
        first_name => 'Kairo',
        last_name => 'Coleman',
        email => 'kairoc@gmail.com',
        grade => 6.3,
        group => 'B',
        ip_addr => '102.201.251.234',
    },
    {
        id => 7,
        first_name => 'Una',
        last_name => 'Roy',
        email => 'unar@gmail.com',
        grade => 3.9,
        group => 'B',
        ip_addr => '234.113.134.27',
    },
    {
        id => 8,
        first_name => 'Damian',
        last_name => 'Roy',
        email => 'unar@gmail.com',
        grade => 10,
        group => 'A',
        ip_addr => '174.123.114.27',
    },
];

my @sorted_students = sort {$a->{first_name} cmp $b->{first_name}} @{$students};

my $filename = 'students.txt';
open( my $fh, '>', $filename) or die "There is a problem with the output file!\n";

foreach my $student (@sorted_students){
    say $fh $student->{id} . ',' .
            $student->{first_name} . ',' . 
            $student->{last_name} . ',' .
            $student->{email} . ',' .
            $student->{grade} . ',' .
            $student->{group} . ',' .
            $student->{ip_addr};
}