#!/usr/bin/perl
use strict;
use warnings;
use POSIX;
use POSIX qw/round/;

&encrypt;

sub encrypt{
    print "Please introduce your text: ";
    chomp (my $text = <STDIN>);
    $text =~ tr/ //ds;
    my $text_length = length ($text);
    while ($text_length > 81 || $text_length == 0){
        print("Your text has more than 81 characters or has no characters at all. Please make it shorter or introduce something.\n");
        chomp ($text = <STDIN>);
        $text =~ tr/ //ds;
        $text_length = length ($text);
    }
    my $square_root_of_length = sqrt($text_length);
    my $rows_number = round($square_root_of_length);
    my $cols_number = ceil($square_root_of_length);
    print "Rows number: " . $rows_number . "; Columns number: " . $cols_number . "\n";

    my @text_matrix;
    
    my ($i, $j) = (0,0);
    for my $c (split //, $text) {
        $text_matrix[$i][$j] = $c;
        if($j < $cols_number - 1){
            $j++;
        }
        else{
            $i++;
            $j=0;
        }
    }
    my $letters = 0;
    print("My matrix: \n");
    for(my $i = 0; $i < $rows_number; $i++){
        for(my $j = 0; $j < $cols_number; $j++){
            unless($letters >= $text_length){
                print $text_matrix[$i][$j] . " ";
            }
            $letters ++;
        }
        print "\n";
    }
    print("Encrypted text: ");
    for(my $j = 0; $j < $cols_number; $j++){
        for(my $i = 0; $i < $rows_number; $i++){
            if (defined $text_matrix[$i][$j]){
                print $text_matrix[$i][$j];
            }
        }
        print " ";
    }
}